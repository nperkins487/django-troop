Django-Troop

Troop Management Software in Django-Python

An open source web app for managing a Boy Scouts of America Troop

Principles: Open Source, Self-Hosting if desired, Troop Administration can easily export/retrieve all data. Clean, modern interface. Flexibility to let scouts do any tasks adults can do if desired. Plan is to include advancement, leadership, events, registration, and roster at first. 

Installation: 

git clone git://git@gitlab.com/troopmanager/django-troop.git

copy the env.example file to .env and provide your own randomly generated secret key

# spin up the container
docker compose up -d --build

# migrate to create the database
docker compose exec web python manage.py migrate

# make sure you have one superuser
docker compose exec web python manage.py createsuperuser

The server should be available at localhost:8000

You can log in to the admin console at localhost:8000/admin/

Troopwebhost: Gather files and place them in django_troop/data/{TROOP}, then run: 
docker compose exec web python manage.py load

Files to Gather
- scouts.csv: obtain from menu->members->troop directory->scout directory ("Open in excel" is download as csv)
- adults.csv: obtain from menu->members->troop directory->adult directory ("Open in excel" is download as csv)
- signoffs.csv: obtain from menu->advancement->maintain advancement->export rank requirement status to excel
- merit_badges.csv: obtain from menu->advancement->advancement status reports->merit badge history by scout by date earned
- merit_badge_signoffs.csv: obtain from menu->advancement->requirement reports->uncompleted merit badge requirements by scout

Troopmaster: Gather files and place them in django_troop/data/{TROOP}, then run:
docker compose exec web python manage.py load

File to gather: 
tm-data.pdf: From Reports->Advancement->Individual History, select all scouts, and turn on all options except ["Omit details on completed ranks", "Include Start/Last Progress", "Include Counselor", "Include Partial MB Remarks", "Mic-O-Say", "Advancement Remarks", "Include Signature Block"]

Scoutbook: TBD


After you gather the files, for troopwebhost it is possible to build the database: 

docker compose exec web python manage.py load

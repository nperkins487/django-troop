#!/bin/sh

main() {
    # Run pre-run user scripts if any
    pre-run
    # Run migrate command
    python manage.py migrate
    # Run Collect static
    python manage.py collectstatic --no-input
    # Get application name string.
    APP=`python manage.py shell -c 'import os; print(os.environ.get("DJANGO_SETTINGS_MODULE").split(".")[0])' 2>/dev/null | tail -n1`
    # Run Gunicorn
    gunicorn "$APP.wsgi:application" -w $GUNICORN_WORKER --timeout $GUNICORN_WORKER_TIMEOUT --bind=$GUNICORN_BIND_IP:$GUNICORN_BIND_PORT
}

main "$@"



$ command cat backend/prod.sh 
#!/bin/bash

set -ex

poetry run python manage.py migrate --no-input
poetry run gunicorn -w 4 -k uvicorn.workers.UvicornWorker -b 0.0.0.0:8000 backend.asgi:application



$ command cat backend/dev.sh
#!/bin/bash

set -x

poetry install --with dev --sync
poetry run python manage.py migrate --noinput
poetry run python manage.py runserver 0.0.0.0:8000

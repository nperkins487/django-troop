# troopmaster

data
  age
  date joined unit
  date of birth
  email
  name
  patrol
  phone
  position
  rank
  rank date
activity totals
  camping nights
  miles hiking
  service hours
rank advancement
  scout
    01a: date
    01b: date
    etc
  tenderfoot
    01a: date
    etc
  etc
leadershio
  position
    end: date
    for_rank: bool
    start: date
merit badges
  badge
    eagle_required: bool
    date: yyyy-mm-dd
national outdoor awards
  award: date
partial merit badges
  badge
    open reqts
    - req1
    - req2
    - req3
    eagle_required: bool
    version: text
special awards
  award: date
training courses:
  course: date


tables 

User - stores user information for people that will be logging in or are tracked by the website. 

Badge - a BSA advancement or award item - contains a name 


Requirement - represents one of the different trackable items needed to be signed off in order to earn a Badge - contains a badge (foreign key to a Bsa badge item) a code (a text field with the code like "5a", text (which is the shortened text of the requirement) and full_text, which is the full requirement


RequirementGroup - a group of requirements used for sorting and reporting, contains a name, and requirements which is a many to many field off Requirement
  RankRequirementGroup - a group of rank requirements only
  MeritBadgeRequirementGroup - a group of merit badge requirements only
  AwardRequirementGroup - a group of award requirements only

Signoff - represents the data stored about a signoff for a scout for a particular requirement. contains a date, a scout (foreign key to Scout object), badge (foreignkey to the badge it's signing off progress toward), and requirement (which is the specific requirement being signed. 

Group - a grouping of user objects, with a name
  Patrol - a special type of group that contains scouts

For developers of django-troop

Please Read the Wiki for style guide and guide to the codebase.

Vision - create an open-source, free, self-hostable equivalent to commercial BSA management software like Troopwebhost, Troopmaster and Scoutbook. 

Short term goals: 

Be able to import from scoutbook data into database

Advancement management - be able to record completion of BSA rank, merit badge and other awards advancement

Goals - 

Troop Roster - be able to manage personal information for members of the troop both scouts and adults

Calendar - be able to manage troop events on a calendar, including having people sign up for events to be listed as wanting to attend and then later as having attended. 

Reports - be able to report on advancement or attendance for scouts or adults. 

Testing - the code should have automated unit and integration tests to ensure it can be easily checked

Technologies - 

Docker - The project should be deployable using docker or docker-compose. Once running, the project should be accessible via https access on a local port. - DONE 

Python - The principle language for the project is Python.  Compatibility with Python 3.10 is a must. 

Django - A "batteries included" web framework for Python.  Compatibility with Django 5.0

HTMX - Augments normal HTML to allow more elements in the document to be interactive and to allow things other than the entire document to be loaded. 

CSS framework - We will use Bootstrap 5.0 and django-crispy-forms

Postgres - from within docker container

Configuration variables will be stored in a TOML file. 

Long-term or exported storage of information for persistance beyond a database will be in CSV. 

Code will be formatted using Black and checked with Ruff with F401 error disabled (due to django creating a bunch of unused imports)

Type hinting is not required (for now). 

We will use Django's provided testing framework. 

Semantic versioning - Major.Minor.Bugfix

Overall license is MIT. 

See docs/essential-features for a roadmap of the phases. I believe the project will be ready for alpha on completion of phase 1. Alpha testing with some close friend troops. Beta testing on Phase 2. Phase 3 and 4 later. 

Due to the questionable pedigree of using AI-generated code, no AI-generated code will be used. 


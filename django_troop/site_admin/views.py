from badges.models import Badge
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin
from scouts.models import User
from signoffs.models import Signoff

from .forms import GroupUpdateForm, InviteForm, UserUpdateForm


class AdminUserListView(PermissionRequiredMixin, ListView):
    template_name = "site_admin/users_list.html"
    permission_required = "scouts.change_user"
    queryset = User.objects


class AdminUserUpdateView(PermissionRequiredMixin, UpdateView):
    model = User
    context_object_name = "user"
    template_name = "site_admin/users_update.html"
    form_class = UserUpdateForm
    permission_required = "scouts.change_user"


class AdminGroupListView(PermissionRequiredMixin, ListView):
    model = Group
    context_object_name = "groups"
    template_name = "site_admin/group_list.html"
    permission_required = ["group.change_group", "group.add_group"]


class AdminGroupUpdateView(PermissionRequiredMixin, UpdateView):
    model = Group
    context_object_name = "group"
    template_name = "site_admin/group_update.html"
    form_class = GroupUpdateForm
    permission_required = "group.change_group"
    success_url = reverse_lazy("site_admin.group_list")


class AdminGroupCreateView(PermissionRequiredMixin, CreateView):
    model = Group
    template_name = "site_admin/group_update.html"
    form_class = GroupUpdateForm
    permission_required = "group.add_group"
    success_url = reverse_lazy("site_admin.group_list")


class AdminGroupDeleteView(PermissionRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy("site_admin.group_list")
    permission_required = "group.change_group"
    template_name = "site_admin/group_confirm_delete.html"


class AdminInviteView(PermissionRequiredMixin, FormView):
    form_class = InviteForm
    template_name = "site_admin/invite_user.html"
    permission_required = "invitation.add_invitation"

from django.contrib import admin

from .models import Patrol

admin.site.register(Patrol)

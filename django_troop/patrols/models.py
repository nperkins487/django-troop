from django.db import models
from django.urls import reverse_lazy


class Patrol(models.Model):
    name = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse_lazy("patrol_detail", kwargs={"pk": self.pk})

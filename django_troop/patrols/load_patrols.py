import csv

from django.conf import settings
from django.core.management.base import BaseCommand
from patrols.models import Patrol
from tm_parser import Parser
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


def load_patrols(path, load_type):
    match load_type:
        case "troopwebhost":
            with open(path / "scouts.csv") as f:
                reader = csv.DictReader(f)
                patrols = set()
                for line in reader:
                    patrols.add(line["Patrol"].strip())

        case "scoutbook":
            with open(path / "personal_data.csv") as f:
                reader = csv.DictReader(f)
                patrols = set()

                for line in tqdm(reader, desc="Patrols"):
                    patrols.add(line["Patrol Name"].strip())

        case "troopmaster":
            p = Parser(path / "tm_data.pdf")
            patrols = set()
            for scout in tqdm(p.scouts.values(), desc="Scouts"):
                patrols.add(scout["Data"].get("Patrol").strip())

    for name in tqdm(patrols, desc="Patrols", total=len(patrols)):
        Patrol.objects.get_or_create(name=name)


class Command(BaseCommand):
    help = "Loads patrols"

    def add_arguments(self, parser):
        parser.add_argument("base_dir", nargs="?", default=CONFIG_DIR, type=str)

    def handle(self, *args, **kwargs):
        load_patrols(path=CONFIG_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(base_dir=CONFIG_DIR, **kwargs)

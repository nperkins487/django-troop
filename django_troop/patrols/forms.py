from django import forms
from icecream import ic
from patrols.models import Patrol
from scouts.models import User


class PatrolForm(forms.ModelForm):
    patrol_members = forms.MultipleChoiceField(
        choices=((None, None)),
    )

    class Meta:
        model = Patrol
        fields = [
            "name",
            "patrol_members",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["patrol_members"] = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            label="Patrol Members",
            choices=self.get_choices(),
        )

        ic(self.fields["patrol_members"])

    def get_choices(self):
        choices = []
        for scout in User.scouts.order_by("patrol", "last_name").all():
            name_str = f"{scout.first_name} {scout.last_name}"
            if scout.patrol:
                name_str += f" ({scout.patrol.name})"
            else:
                name_str += " (Not assigned)"
            choices.append((scout.pk, name_str))
        return choices

from badges.models import Badge
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Count, Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, FormMixin, UpdateView
from django.views.generic.list import ListView
from icecream import ic
from patrols.forms import PatrolForm
from patrols.models import Patrol
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff

TOP_RANK_NEEDS = 5


class PatrolUpdateView(PermissionRequiredMixin, UpdateView, FormMixin):
    model = Patrol
    form_class = PatrolForm
    permission_required = ["patrols.change_patrol"]

    def form_valid(self, form):
        print(form.cleaned_data.get("patrol_members"))
        form.instance.patrol_members.set([])
        for scout in form.cleaned_data.get("patrol_members"):
            form.instance.patrol_members.add(User.objects.get(pk=scout))
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["initial"].update(self.get_current_scouts())
        ic(kwargs)
        return kwargs

    def get_current_scouts(self):
        if self.object:
            ic()
            ic(self.object)
            current = []
            for scout in (
                User.scouts.filter(patrol=self.object)
                .order_by("patrol", "last_name")
                .all()
            ):
                name_str = f"{scout.first_name} {scout.last_name}"
                if scout.patrol:
                    name_str += f" ({scout.patrol.name})"
                else:
                    name_str += " (Not assigned)"
                current.append((scout.pk, name_str))
            ic()
            return {"patrol_members": current}
        else:
            ic()
            return {"patrol_members": None}


class PatrolCreateView(PermissionRequiredMixin, CreateView, FormMixin):
    model = Patrol
    form_class = PatrolForm
    permission_required = ("patrols.add_patrol",)

    def form_valid(self, form):
        self.object = form.save()
        for scout in form.cleaned_data.get("patrol_members"):
            form.instance.patrol_members.add(User.objects.get(pk=scout))
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

        FormMixin.form_valid(self, form)


class PatrolDeleteView(PermissionRequiredMixin, DeleteView):
    model = Patrol
    success_url = reverse_lazy("patrol_list")
    permission_required = ("patrols.change_patrol",)


class PatrolListView(LoginRequiredMixin, ListView):
    model = Patrol
    context_object_name = "patrols"
    ordering = "name"


class PatrolDetailView(LoginRequiredMixin, DetailView):
    model = Patrol
    context_object_name = "patrol"
    template_name = "patrols/patrol_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["scouts"] = context["patrol"].patrol_members

        context["scout_count"] = context["scouts"].count()

        rank_needs = {}

        for rank in Badge.objects.filter(badge_type="rank", order__in=range(4)).all():
            rank_needs[rank.order] = (
                Requirement.objects.filter(
                    badge=rank, signoffs__scout__in=context["scouts"].all()
                )
                .exclude(labels__name__contains="Excludable")
                .annotate(count=Count("signoffs__id", filter=Q(signoffs__signed=False)))
                .order_by("-count")
                .exclude(count__lte=0)
                .values(
                    "count",
                    "code",
                    "text",
                    "badge__order",
                    "badge",
                    "badge__id",
                    "badge__name",
                )[:TOP_RANK_NEEDS]
            )

        context["rank_needs"] = rank_needs

        context["advancement_scouts"] = (
            context["scouts"]
            .filter(current_rank__order__lt=4)
            .order_by("last_name", "first_name")
            .all()
        )

        context["signoffs"] = Signoff.objects.filter(
            requirement__badge__badge_type="rank",
            scout__in=context["scouts"].all(),
            requirement__badge__order__lt=4,
            requirement__badge__order__gte=0,
            scout__current_rank__order__lt=4,
        ).order_by(
            "requirement__badge",
            "requirement__code",
            "scout__last_name",
            "scout__first_name",
        )

        context["requirements"] = (
            Requirement.objects.filter(badge__badge_type="rank")
            .order_by("badge", "code")
            .all()
        )

        return context

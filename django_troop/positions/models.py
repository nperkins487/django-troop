from django.db import models


class Position(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, null=True, blank=True)
    position_type = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        choices=(("scout", "scout"), ("adult", "adult"), ("either", "either")),
    )

    def __str__(self):
        return f"{self.name}"

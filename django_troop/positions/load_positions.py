from pathlib import Path

import toml
from django.conf import settings
from django.core.management.base import BaseCommand
from positions.models import Position
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


def load_positions(path):
    data = toml.load(path)
    for name, position_list in tqdm(data.items(), desc="Positions"):
        position_type = (
            "scout" if "Scout" in name else "adult" if "Adult" in name else "either"
        )
        for item in position_list:
            position, created = Position.objects.get_or_create(
                name=item, position_type=position_type
            )


class Command(BaseCommand):
    help = "Loads Position definitions"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_positions(CONFIG_DIR / "positions.toml")


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

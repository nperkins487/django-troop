from django.urls import path

from . import views
from .views import PositionDetailView, PositionListView

urlpatterns = [
    path("<int:pk>", PositionDetailView.as_view(), name="position_detail"),
    path("", PositionListView.as_view(), name="positions_list"),
    path("edit/<int:pk>/", views.position_edit, name="positions_update"),
    path("edit/new/", views.position_edit, name="positions_create"),
]

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings

"""
django-troop works on an invite-only system
users are not allowed to sign up for a new account - they are invited by the troop's admins

These adapters disable the is_open_for_signup method, by turning it off when the appropriate
setting is disabled 

see https://stackoverflow.com/questions/17923692/turn-off-user-social-registration-in-django-allauth
"""


class CustomAccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        allow_signups = super(CustomAccountAdapter, self).is_open_for_signup(request)
        return getattr(settings, "ACCOUNT_ALLOW_SIGNUPS", allow_signups)


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, sociallogin):
        allow_signups = super(CustomSocialAccountAdapter, self).is_open_for_signup(
            request, sociallogin
        )
        return getattr(settings, "SOCIALACCOUNT_ALLOW_SIGNUPS", allow_signups)

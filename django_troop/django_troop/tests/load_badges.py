from pathlib import Path

import toml

data = toml.load(Path("..") / "data" / "example_troop" / "requirements.toml")

print(data)

from django.contrib import admin

from .models import Requirement, RequirementLabel

admin.site.register(Requirement)
admin.site.register(RequirementLabel)

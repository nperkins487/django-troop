from django.db import models


class RequirementLabel(models.Model):
    name = models.CharField(max_length=100)


class Requirement(models.Model):
    badge = models.ForeignKey(
        "badges.Badge", on_delete=models.CASCADE, related_name="requirements"
    )
    code = models.CharField(max_length=20)
    text = models.CharField(max_length=200, null=True)
    full_text = models.CharField(max_length=1000, null=True)
    equivalent_requirements = models.CharField(max_length=100, null=True)
    labels = models.ManyToManyField("RequirementLabel", related_name="requirements")

    def __str__(self):
        return f"{self.__class__.__name__} {self.badge.name} {self.code}"

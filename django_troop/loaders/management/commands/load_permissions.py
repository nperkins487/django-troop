from pathlib import Path

import toml
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from icecream import ic
from positions.models import Position
from scouts.models import User
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR

content_model = ContentType.objects.get_for_model(User)


def load_permissions(path):
    data = toml.load(path)
    for group_name, permissions in tqdm(data.items(), desc="Permissions"):
        group, created = Group.objects.get_or_create(name=group_name)
        for permission in permissions:
            ic(group_name, permission)
            permission_to_add = Permission.objects.get(codename=permission["codename"])
            group.permissions.add(permission_to_add)
        group.save()


class Command(BaseCommand):
    help = "Loads Position definitions"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_permissions(CONFIG_DIR / "groups.toml")


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

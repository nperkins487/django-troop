from pathlib import Path

import toml
from badges.models import Badge, get_mb_name
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


def load_merit_badges(path, load_type):
    mb_data = toml.load("data/common_files/mb_data.toml")

    for merit_badge, data in tqdm(mb_data.items()):
        badge, created = Badge.objects.get_or_create(
            name=get_mb_name(merit_badge),
            badge_type="merit_badge",
            eagle_required=data["eagle required"],
        )
        requirement, created = Requirement.objects.get_or_create(
            badge=badge, code="Completion", text=f"{badge.name} completion"
        )


class Command(BaseCommand):
    help = "Loads Merit Badges"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_merit_badges(CONFIG_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

#!/usr/bin/env bash

set -exo pipefail

if [[ $# -ne 2 ]]; then
	echo "wrong number of arguments"
	echo "args: TROOP PORT"
	exit 1
fi

BASEDIR=${HOME}/projects/django-troop/django_troop
cd $BASEDIR

DEBUG=False
TROOP=$1
PORT=$2

mkdir -p data/$TROOP
cd data/$TROOP/
ln -sf ../common_files/* .

cat << END_OF_ENV > .env
TROOP=${TROOP}
DJANGO_ALLOWED_HOSTS=${TROOP}.troopmanager.org localhost 127.0.0.1 [::1]
PORT=${PORT}
END_OF_ENV

touch ${TROOP}-db.sqlite3


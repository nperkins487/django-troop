#!/usr/bin/env bash

set -exo pipefail

if [[ $# -ne 7 ]]; then 
	echo "Wrong number of arguments"
	echo "arguments: TROOP PORT USER EMAIL IP REMOTE_SSH_SERVER REMOTE_SSH_USER"
	exit 1 
fi

BASEDIR=${HOME}/projects/django-troop/django_troop
cd $BASEDIR

DEBUG=False
TROOP=$1
PORT=$2
USER=$3
EMAIL=$4
IP=$5
REMOTE_SSH_SERVER=$6
REMOTE_SSH_USER=$7

set +x
export DJANGO_SUPERUSER_PASSWORD=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13; echo)
set -x

mkdir -p data/${TROOP}
cd data/${TROOP}/
ln -sf ../common_files/* .

cat << END_OF_ENV > .env
TROOP=${TROOP}
DJANGO_ALLOWED_HOSTS=${TROOP}.troopmanager.org localhost 127.0.0.1 [::1]
PORT=${PORT}
END_OF_ENV

touch ${TROOP}-db.sqlite3

cd $BASEDIR
docker-compose -p ${TROOP} --env-file data/${TROOP}/.env up -d --build

echo "Sleeping for 10 seconds…"
sleep 10
echo "Completed"

docker-compose -p $TROOP --env-file data/${TROOP}/.env exec web python manage.py createsuperuser --noinput --username ${USER} --email ${EMAIL}


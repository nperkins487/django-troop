#!/usr/bin/env bash

set -exo pipefail

if [[ $# -ne 3 ]]; then
	echo "wrong number of arguments"
	echo "arguments: TROOP PORT USER EMAIL IP"
	exit 1
fi

BASEDIR=${HOME}/projects/django-troop/django_troop
cd $BASEDIR

DEBUG=False
TROOP=$1
PORT=$2
IP=$3

mkdir -p data/$TROOP
cd data/$TROOP/
ln -sf ../common_files/* .

cat << END_OF_ENV > .env
TROOP=${TROOP}
DJANGO_ALLOWED_HOSTS=${TROOP}.troopmanager.org localhost 127.0.0.1 [::1]
PORT=${PORT}
END_OF_ENV

cat > nginx.conf << END_OF_NGINX
server {
	server_name ${TROOP}.troopmanager.org;
	location / {
		include proxy_params;
		proxy_pass http://${IP}:${PORT};
	}
}

server {
    if (\$host = ${TROOP}.troopmanager.org) {
        return 301 https://\$host\$request_uri;
    } # managed by Certbot


	listen 80;
	server_name ${TROOP}.troopmanager.org;
    return 404; # managed by Certbot

}
END_OF_NGINX

sudo cp nginx.conf /etc/nginx/conf.d/${TROOP}.nginx.conf
sudo certbot -n --nginx -d ${TROOP}.troopmanager.org

sudo systemctl reload nginx


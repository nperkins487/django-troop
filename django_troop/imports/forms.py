from django import forms


class TroopmasterImportForm(forms.Form):
    input_file = forms.FileField(
        label="Please Select your individual history PDF file",
        required=True,
        allow_empty_file=False,
    )


class TroopwebhostImportForm(forms.Form):
    input_scouts_file = forms.FileField(required=True, allow_empty_file=False)
    input_adults_file = forms.FileField(required=False)
    input_signoffs_file = forms.FileField(required=False)
    input_merit_badges_file = forms.FileField(required=False)
    input_merit_badges_partials_file = forms.FileField(required=False)


class ScoutbookImportForm(forms.Form):
    scouts_file = forms.FileField(
        label="Select your scout information file (CSV)", required=False
    )
    advancement_file = forms.FileField(
        label="Select your advancement information file (CSV)", required=True
    )

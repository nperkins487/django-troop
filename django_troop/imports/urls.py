from django.urls import path

from . import views

urlpatterns = [
    path("", views.ImportView.as_view(), name="imports"),
    path(
        "troopwebhost/",
        views.TroopwebhostImportView.as_view(),
        name="troopwebhost_imports",
    ),
    path(
        "troopmaster/",
        views.TroopmasterImportView.as_view(),
        name="troopmaster_imports",
    ),
    path(
        "troopmaster/success/",
        views.troopmaster_import_success,
        name="troopmaster_success",
    ),
    path("scoutbook/", views.ScoutbookImportView.as_view(), name="scoutbook_imports"),
    path(
        "scoutbook/success/",
        views.scoutbook_import_success,
        name="scoutbook_success",
    ),
]

import imports.forms as Forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from icecream import ic
from leaders.load_leaders import load_troopmaster_leaders
from scoutbook_parser import Parser as SBParser
from scouts.load_scouts import (
    load_scoutbook_scout,
    load_troopmaster_scout,
    record_current_ranks,
)
from signoffs.load_signoffs import load_scoutbook_signoffs, load_troopmaster_signoffs
from tm_parser import Parser as TMParser
from tqdm import tqdm


class ImportView(PermissionRequiredMixin, TemplateView):
    template_name = "imports/import.html"
    permission_required = ("scouts.import_users",)


class TroopwebhostImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/troopwebhost_import.html"
    form_class = Forms.TroopwebhostImportForm
    permission_required = ("scouts.import_users",)


class TroopmasterImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/troopmaster_import.html"
    form_class = Forms.TroopmasterImportForm
    success_url = reverse_lazy("scouts_list")
    permission_required = ("scouts.import_users",)

    def form_valid(self, form):
        parsed_scouts = TMParser(stream=form.cleaned_data["input_file"].file)
        for scout in tqdm(
            parsed_scouts.scouts.values(),
            desc="Scouts",
            total=len(parsed_scouts.scouts.values()),
        ):
            load_troopmaster_scout(scout)
            load_troopmaster_signoffs(scout)
            load_troopmaster_leaders(scout)
            record_current_ranks(scout)

        messages.success(
            self.request,
            f"Successfully imported or updated {len(parsed_scouts.scouts.values())} scouts",
        )

        return HttpResponseRedirect(reverse_lazy("scouts_list"))


class ScoutbookImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/scoutbook_import.html"
    form_class = Forms.ScoutbookImportForm
    success_url = reverse_lazy("scouts_list")
    permission_required = ("scouts.import_users",)

    def form_valid(self, form):
        decoded_personal = (
            form.cleaned_data["scouts_file"].file.read().decode("utf-8").splitlines()
        )
        decoded_advancement = (
            form.cleaned_data["advancement_file"]
            .file.read()
            .decode("utf-8")
            .splitlines()
        )
        parsed_scouts = SBParser(
            personal=decoded_personal, advancement=decoded_advancement
        )

        for scout in tqdm(
            parsed_scouts.scouts.values(),
            desc="Scouts",
            total=len(parsed_scouts.scouts.values()),
        ):
            load_scoutbook_scout(scout)
            load_scoutbook_signoffs(scout)
            record_current_ranks(scout)

        messages.success(
            self.request,
            f"Successfully imported or updated {len(parsed_scouts.scouts.values())} scouts",
        )

        return HttpResponseRedirect(reverse_lazy("scouts_list"))


@login_required
def troopmaster_import_success(request):
    return HttpResponse("got file")


@login_required
def scoutbook_import_success(request):
    return HttpResponse("got file")

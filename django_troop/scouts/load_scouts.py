import csv
from pathlib import Path

from badges.models import Badge
from django.conf import settings
from django.core.cache import cache
from django.core.management.base import BaseCommand
from icecream import ic
from leaders.models import Leader
from patrols.models import Patrol
from positions.models import Position
from scouts.models import User
from signoffs.load_signoffs import start_a_badge
from signoffs.models import Signoff
from tm_parser import Parser
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


RANK_EQUIVALENTS = {
    "2nd Bronze Palm": "Bronze Palm (2)",
    "2nd Gold Palm": "Gold Palm (2)",
    "2nd Silver Palm": "Silver Palm (2)",
}


def load_scouts(path, load_type):
    match load_type:
        case "scoutbook":
            ...
        case "troopwebhost":
            with open(path / "scouts.csv", encoding="utf-8-sig") as f:
                number = len(f.readlines())
                f.seek(0)
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Scouts", total=number):
                    load_troopwebhost_scout(line)
        case "troopmaster":
            p = Parser(path / "tm_data.pdf")
            for scout in tqdm(
                p.scouts.values(), desc="Scouts", total=len(p.scouts.values())
            ):
                load_troopmaster_scout(scout)


def load_troopwebhost_scout(line):
    scout_obj, created = cache.get_or_set(line, get_scout_obj(line))
    scout_obj.patrol = get_patrol(line)
    ic(scout_obj.patrol)
    scout_obj.age = get_age(line)
    ic(scout_obj.age)
    scout_obj.current_rank = get_current_rank(line)
    ic(scout_obj.current_rank)
    start_next_ranks(scout_obj)
    ic("got the next ranks")
    record_leadership(scout_obj, line)
    ic("got the leadership")
    scout_obj.save()


def load_troopmaster_scout(scout):
    if "Data" in scout and scout["Data"]["Last Name"] and scout["Data"]["First Name"]:
        scout_obj, created = cache.get_or_set(
            f"{scout.get('Data').get('Last Name')}{scout.get('Data').get('First Name')}",
            get_scout_obj(scout),
        )
    if not scout_obj:
        scout_obj, created = get_scout_obj(scout)

    scout_obj.patrol = get_patrol(scout)
    ic(scout_obj.patrol)
    scout_obj.current_rank = get_current_rank(scout)
    ic(scout_obj.current_rank)
    scout_obj.age = get_age(scout)
    ic(scout_obj.age)
    scout_obj.initial_signoffs(scout_obj.current_rank.order + 1)
    ic(scout_obj)
    scout_obj.save()


def load_scoutbook_scout(scout):
    if "Data" in scout and scout["Data"]["Last Name"] and scout["Data"]["First Name"]:
        scout_obj, created = cache.get_or_set(
            f"{scout["Data"]["Last Name"]}{scout["Data"]["First Name"]}",
            get_scout_obj(scout),
        )
    if not scout_obj:
        scout_obj, created = get_scout_obj(scout)
        cache.set(
            f"{scout["Data"]["Last Name"]}{scout["Data"]["First Name"]}", scout_obj
        )
    scout_obj.patrol = get_patrol(scout)
    ic(scout_obj.patrol)
    scout_obj.current_rank = get_current_rank(scout, scout_object=scout_obj)
    ic(scout_obj.current_rank)
    scout_obj.age = get_age(scout)
    ic(scout_obj.age)
    scout_obj.initial_signoffs(scout_obj.current_rank.order + 1)
    ic(scout_obj)
    scout_obj.save()


def record_leadership(scout_obj, line):
    if line.get("Leadership"):
        positions = line.get("Leadership").split(", ")
        for position_name in positions:
            try:
                position = Position.objects.get(name=position_name)
            except Position.DoesNotExist:
                position, created = Position.objects.get_or_create(
                    name=position_name, position_type="scout"
                )
            finally:
                leader, created = Leader.objects.get_or_create(
                    user=scout_obj, position=position
                )
                leader.save()


def get_scout_obj(scout):
    if "Data" in scout and scout["Data"].get("Name"):
        scout_name = User.name_from_string(scout["Data"]["Name"])
        first_name = scout_name.get("first_name")
        last_name = scout_name.get("last_name")
        nick_name = scout_name.get("nick_name")
    else:
        first_name = scout["Data"].get("First Name")
        last_name = scout["Data"].get("Last Name")
        nick_name = scout["Data"].get("Nick Name")
    username = f"{first_name}{last_name}"
    scout_obj, created = User.scouts.get_or_create(
        username=username, last_name=last_name, first_name=first_name
    )
    scout_obj.nick_name = nick_name
    scout_obj.user_type = "scout"
    scout_obj.save()
    return scout_obj, created


def get_current_rank(scout, scout_object=None):
    if "Data" in scout and scout["Data"].get("Rank"):
        current_rank = scout["Data"].get("Rank").strip()
    elif "Rank" in scout:
        if scout.get("Rank") in RANK_EQUIVALENTS:
            current_rank = RANK_EQUIVALENTS[scout.get("Rank").strip()]
        else:
            current_rank = scout.get("Rank").strip()
    else:
        current_rank = "No Rank"
    return Badge.objects.get(badge_type="rank", name=current_rank)


def start_next_ranks(scout_obj):
    # start the scout off on their next rank. If the next rank is below first class,
    # start them on all ranks through first class.
    next_rank_index = scout_obj.current_rank.order + 1

    for i in range(next_rank_index, 4):
        next_rank = Badge.objects.get(badge_type="rank", order=i)
        start_a_badge(scout_obj, next_rank)
    if 3 < next_rank_index <= 6:  # 6 is Eagle
        next_rank = Badge.objects.get(badge_type="rank", order=next_rank_index)
        start_a_badge(scout_obj, next_rank)


def get_patrol(scout):
    patrol = scout.get("Patrol") or scout["Data"].get("Patrol")
    if patrol:
        patrol, created = Patrol.objects.get_or_create(name=patrol.strip())
        if created:
            patrol.save()
        return patrol
    return None


def get_age(scout):
    try:
        age = int(scout["Data"].get("Age", "1"))
    except (TypeError, ValueError) as e:
        age = 1
        print(f"invalid scout age: {e}")
    return age


def record_current_ranks(scout):

    # obtain the scout object in question
    scout_obj = User.scouts.get(
        last_name=scout["Data"]["Last Name"], first_name=scout["Data"]["First Name"]
    )

    # obtain the signoffs for this scout for all ranks that have been completed
    rank_signoffs = Signoff.final_signoffs.filter(scout=scout_obj, signed=True).all()

    for signoff in rank_signoffs:
        # loop through and add these to the completed ranks for that scout
        scout_obj.completed_ranks.add(signoff)

        # and make sure all that badge's signoffs are at least signed=True
        scout_obj.signoff_completed_badge(signoff.requirement.badge)

    if rank_signoffs:
        # get the current rank they've completed (it's the one with the highest index)
        current_rank_index = max(
            (signoff.requirement.badge.order for signoff in rank_signoffs)
        )
    else:
        current_rank_index = -1

    scout_obj.current_rank = Badge.objects.get(
        badge_type="rank", order=current_rank_index
    )
    scout_obj.save()


class Command(BaseCommand):
    help = "Loads scouts, patrols"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_scouts(CONFIG_DIR, kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

import csv
from pathlib import Path

from django.conf import settings
from django.core.management.base import BaseCommand
from leaders.models import Leader
from positions.models import Position
from scouts.models import User
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


def load_adults(path, load_type):
    match load_type:
        case "scoutbook":
            ...
        case "troopwebhost":
            with open(path / "adults.csv", encoding="utf-8-sig") as f:
                number = len(f.readlines())
                f.seek(0)
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Adults", total=number):
                    load_troopwebhost_adult(line)
        case "troopmaster":
            ...


def load_troopwebhost_adult(line):
    adult_name = User.name_from_string(line["Name"])

    adult, created = User.objects.get_or_create(
        first_name=adult_name.get("first_name"),
        last_name=adult_name.get("last_name"),
        nick_name=adult_name.get("nick_name"),
        user_type="adult",
        username=f"{adult_name.get('last_name')}{adult_name.get('first_name')}",
    )

    if line.get("Leadership"):
        positions = line.get("Leadership").split(", ")

        for position_name in positions:
            try:
                position = Position.objects.get(name=position_name)
            except Position.DoesNotExist:
                position, created = Position.objects.get_or_create(
                    name=position_name, position_type="adult"
                )
            finally:
                leader, created = Leader.objects.get_or_create(
                    user=adult, position=position
                )
                leader.save()

    adult.save()


class Command(BaseCommand):
    help = "Loads Adults"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_adults(CONFIG_DIR, kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

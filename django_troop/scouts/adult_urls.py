from django.urls import path

from . import views

urlpatterns = [
    path("", views.AdultListView.as_view(), name="adult_list"),
    path("<int:pk>", views.AdultDetailView.as_view(), name="adult_detail"),
    path("edit/<int:pk>/", views.AdultUpdateView.as_view(), name="adult_edit"),
    path("edit/new/", views.AdultCreateView.as_view(), name="adult_create"),
]

from django.urls import path

from . import views

urlpatterns = [
    path("edit/<int:pk>/", views.UserUpdateView.as_view(), name="user_edit"),
]

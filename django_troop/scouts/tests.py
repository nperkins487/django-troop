from badges.models import Badge
from django.test import TestCase
from django.urls import reverse
from patrols.models import Patrol
from scouts.models import User

# Create your tests here.


class AllScoutPageTests(TestCase):
    def setUp(self):
        url = reverse("scouts_list")
        self.response = self.client.get(url)

    def test_url_exists_at_current_location(self):
        self.assertEqual(self.response.status_code, 200)

    def test_all_scouts_template(self):
        self.assertTemplateUsed(self.response, "scouts/scouts_list.html")


class OneScoutPageTests(TestCase):
    def setUp(self):
        self.rank = Badge(badge_type="rank", name="Test Rank", order=0)
        self.patrol = Patrol(name="Test Patrol")
        self.rank2 = Badge(badge_type="rank", name="First Class", order=1)
        self.rank.save()
        self.rank2.save()
        self.patrol.save()
        self.scout = User(
            last_name="Henson",
            first_name="Jim",
            patrol=self.patrol,
            user_type="scout",
            current_rank=self.rank,
        )
        self.scout.save()
        url = reverse("scouts_detail", args=(self.scout.id,))
        self.response = self.client.get(url)

    def test_url_exists_at_current_location(self):
        self.assertEqual(self.response.status_code, 200)

    def test_one_scout_template(self):
        self.assertTemplateUsed(self.response, "scouts/scouts_detail.html")

    def test_one_scout_page_content(self):
        self.assertContains(self.response, "Jim H")

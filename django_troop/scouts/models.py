import re

from badges.models import Badge
from django.contrib.auth.models import AbstractUser, UserManager
from django.core.cache import cache
from django.db import models
from django.urls import reverse_lazy
from signoffs.models import Signoff


class ScoutManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type="scout")


class AdultManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type="adult")


class User(
    AbstractUser,
):
    user_type = models.CharField(
        max_length=10,
        default="other",
        choices=(("scout", "scout"), ("adult", "adult"), ("other", "other")),
    )
    groups = models.ManyToManyField("patrols.Patrol", related_name="group_members")
    first_name = models.CharField(max_length=150, blank=True, null=True)
    last_name = models.CharField(max_length=150)
    middle_name = models.CharField(max_length=150, blank=True, null=True)
    nick_name = models.CharField(max_length=150, blank=True, null=True)
    suffix = models.CharField(max_length=150, blank=True, null=True)

    pronouns = models.CharField(max_length=150, blank=True, null=True)

    age = models.IntegerField(null=True, blank=True)
    patrol = models.ForeignKey(
        "patrols.Patrol",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="patrol_members",
    )
    current_rank = models.ForeignKey(
        "badges.Badge",
        on_delete=models.SET_NULL,
        null=True,
        related_name="current_rank_scouts",
    )
    parents = models.ManyToManyField("self", blank=True)
    children = models.ManyToManyField("self", blank=True)

    completed_ranks = models.ManyToManyField(
        "signoffs.Signoff",
        related_name="completed_ranks_scouts",
        blank=True,
    )

    completed_merit_badges = models.ManyToManyField(
        "signoffs.Signoff",
        related_name="completed_merit_badges_scouts",
        blank=True,
    )

    class Meta:
        permissions = [
            (
                "import_users",
                "Can import users from source files",
            ),
            (
                "edit_user_permissions",
                "Can edit user permissions",
            ),
            (
                "invite_users",
                "Can invite new users",
            ),
        ]

    objects = UserManager()
    scouts = ScoutManager()
    adults = AdultManager()

    def get_absolute_url(self):
        if self.user_type == "scout":
            return reverse_lazy("scouts_detail", kwargs={"pk": self.pk})
        elif self.user_type == "adult":
            return reverse_lazy("adult_detail", kwargs={"pk": self.pk})

    @classmethod
    def name_from_string(cls, text):
        # Matches "Perkins, Michael "Mike""
        pat1 = re.compile(r'^(.*), (.*?)( "(.*)")?$')

        # Matches "Michael Perkins"
        pat2 = re.compile(r"^(.*) (.*)$")

        if m := pat1.match(text.strip()):
            last_name = m.group(1).strip()
            first_name = m.group(2).strip()
            nick_name = m.group(4).strip() if m.group(4) else None

        elif m := pat2.match(text.strip()):
            last_name = m.group(2).strip()
            first_name = m.group(1).strip()
            nick_name = None

        else:
            last_name = text

        return {
            "last_name": last_name,
            "first_name": first_name,
            "nick_name": nick_name,
        }

    @property
    def current_rank_date(self):
        return cache.get_or_set(
            f"current_rank {self.pk}",
            Signoff.objects.get(scout=self, requirement=self.current_rank.final).date,
        )

    @property
    def next_rank(self):

        return Badge.objects.get(badge_type="rank", order=self.current_rank.order + 1)

    def percent_rank_done(self):
        try:
            return (
                Signoff.objects.filter(
                    scout=self, requirement__badge=self.next_rank, signed=True
                ).count()
                / Signoff.objects.filter(
                    scout=self, requirement__badge=self.next_rank
                ).count()
            ) * 100.0
        except ZeroDivisionError:
            return 0

    def whole_name(self):
        if self.nick_name:
            return f'{self.first_name} "{self.nick_name}" {self.last_name}'
        return f"{self.first_name} {self.last_name}"

    def safe_name(self):
        return f"{self.first_name} {self.last_name[0]}"

    def badge_complete(self, badge):
        return Signoff.complete(self, badge)

    def start_badge(self, badge):
        requirements = cache.get_or_set(
            f"all_requirements for badge {badge.pk}", badge.requirements.all()
        )
        for requirement in requirements:
            signoff, created = Signoff.objects.get_or_create(
                scout=self, requirement=requirement
            )

    def initial_signoffs(
        self, highest_rank_index=3
    ):  # normally we'll provide the signoffs through first class
        for rank_order in range(
            -1, highest_rank_index + 1
        ):  # provide signoffs for each rank
            badge = cache.get_or_set(
                f"rank_badge {rank_order}", Badge.objects.get(order=rank_order)
            )
            self.start_badge(badge=badge)

    def signoff_completed_badge(self, badge):
        for requirement in badge.requirements.all():
            signoff, created = Signoff.objects.get_or_create(
                scout=self, requirement=requirement
            )
            if not signoff.date:
                signoff.signed = True
                signoff.save()

from pathlib import Path

import toml
from badges.models import Badge
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR


def load_ranks(path, load_type):
    data = toml.load(path)

    for rank, rank_data in tqdm(data.items(), desc="Ranks", total=len(data.items())):
        r, created = Badge.objects.get_or_create(
            name=rank, badge_type="rank", order=rank_data["rank_order"]
        )
        if created:
            r.save()

        final_code = rank_data["final_code"]
        for code, more_data in tqdm(rank_data["requirements"].items(), desc=rank):
            req, created = Requirement.objects.get_or_create(
                badge=r,
                code=code,
                text=more_data["text"],
                full_text=more_data["full_text"],
                equivalent_requirements=more_data.get("equivalent_requirements"),
            )
            if created:
                req.save()

            if code == final_code:
                r.final = req
                r.save()


class Command(BaseCommand):
    help = "Loads ranks and requirements"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])
        local_config = toml.load(CONFIG_DIR / "local_settings.toml")
        load_type = local_config.get("IMPORT_TYPE", kwargs.get("load_type", None))

        load_ranks(CONFIG_DIR / "requirements.toml", load_type=load_type)


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

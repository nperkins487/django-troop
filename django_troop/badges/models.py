import re

from django.db import models
from signoffs.models import Signoff

ALTERNATE_NAMES = {
    "Signs, Signals & Codes": "Signs, Signals and Codes",
    "Motor Boating": "Motorboating",
}


class Badge(models.Model):
    name = models.CharField(max_length=100)

    badge_type = models.CharField(
        max_length=50,
        choices=(("rank", "rank"), ("merit_badge", "merit_badge"), ("award", "award")),
    )

    def __str__(self):
        return f"{self.badge_type} {self.name}"

    order = models.SmallIntegerField(null=True)
    final = models.ForeignKey(
        "requirements.Requirement",
        null=True,
        on_delete=models.SET_NULL,
        related_name="final_requirement",
    )

    eagle_required = models.BooleanField(default=False)

    class Meta:
        permissions = [
            ("assign_merit_badge", "Can assign a new merit badge for a scout to begin"),
        ]

    def __lt__(self, other):
        if self.order and self.order < other.order:
            return True
        return False

    def complete(self, scout):
        return Signoff.complete(scout, self)


def get_mb_name(line):
    pat = re.compile(r"(.*) \(.*\)\*?")
    match = pat.match(line.strip())
    if match:
        mb_name = match.group(1)
    else:
        mb_name = line.strip(r"*")

    if mb_name in ALTERNATE_NAMES:
        mb_name = ALTERNATE_NAMES[mb_name]

    return mb_name

from badges.models import Badge
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff


class RankListView(LoginRequiredMixin, ListView):
    context_object_name = "ranks"
    template_name = "badges/rank_list.html"
    ordering = ["order"]

    queryset = Badge.objects.filter(badge_type="rank").all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for rank in context["ranks"]:
            total_requirements = Requirement.objects.filter(badge=rank).count()
            # get scouts working on this rank
            rank.scouts = User.scouts.filter(current_rank__order=rank.order - 1).all()

            for scout in rank.scouts:
                requirements_earned = Signoff.signed_requirements.filter(
                    scout=scout,
                    requirement__badge=rank,
                ).count()

                scout.requirements_needed = total_requirements - requirements_earned
                scout.percent_rank_done = (
                    requirements_earned / total_requirements * 100.0
                )

        context["ranks"] = context["ranks"][1:]
        return context


class RankDetailView(LoginRequiredMixin, DetailView):
    queryset = Badge.objects.filter(badge_type="rank")
    context_object_name = "rank"
    template_name = "badges/rank_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["requirements"] = (
            Requirement.objects.filter(badge=context["rank"]).order_by("code").all()
        )

        if context["rank"].order <= 3:
            for requirement in context["requirements"]:
                scouts_needing = User.scouts.filter(
                    current_rank__order__lt=requirement.badge.order
                ).count()
                requirement.count = (
                    scouts_needing
                    - Signoff.signed_requirements.filter(
                        requirement__badge=context["rank"],
                        requirement=requirement,
                        scout__current_rank__order__lt=requirement.badge.order,
                    ).count()
                )
        if context["rank"].order > 3:
            for requirement in context["requirements"]:
                scouts_needing = User.scouts.filter(
                    current_rank__order=requirement.badge.order - 1
                ).count()
                requirement.count = (
                    scouts_needing
                    - Signoff.signed_requirements.filter(
                        requirement__badge=context["rank"],
                        requirement=requirement,
                        scout__current_rank__order=requirement.badge.order - 1,
                    ).count()
                )

        return context

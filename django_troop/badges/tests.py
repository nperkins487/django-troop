from badges.models import Badge
from django.test import TestCase
from django.urls import reverse

# Create your tests here.


class BadgeTests(TestCase):
    def setUp(self):
        url = reverse("rank_list")
        self.response = self.client.get(url)

    def test_url_exists_at_current_location(self):
        self.assertEqual(self.response.status_code, 200)

    def test_badge_list_template(self):
        self.assertTemplateUsed(self.response, "badges/rank_list.html")


class BadgeDetailTests(TestCase):
    def setUp(self):
        self.rank = Badge.objects.create(badge_type="rank", name="First Class", order=3)
        url = reverse("rank_detail", args=(self.rank.id,))
        self.response = self.client.get(url)

    def test_url_exists_at_current_location(self):
        self.assertEqual(self.response.status_code, 200)

    def test_badge_detail_template(self):
        self.assertTemplateUsed(self.response, "badges/rank_detail.html")

    def test_badge_detail_content(self):
        self.assertContains(self.response, "First Class")

from django.conf import settings
from icecream import ic
from leaders.models import Leader
from positions.models import Position
from scouts.models import User

POSITION_EQUIVALENTS = {}


def load_troopmaster_leaders(scout):
    scout_name = User.name_from_string(scout["Data"]["Name"])

    scout_obj = User.scouts.get(
        first_name=scout_name.get("first_name"),
        last_name=scout_name.get("last_name"),
        user_type="scout",
    )

    for item in scout.get("Leadership", []):
        for title, data in item.items():
            position, created = Position.objects.get_or_create(
                name=title, position_type="scout"
            )
            Leader.objects.create(
                user=scout_obj,
                start_date=data["Start Date"],
                end_date=data["End Date"],
                position=position,
            )

import django_filters
from django.forms import widgets
from leaders.models import Leader


class LeaderFilter(django_filters.FilterSet):
    end_date = django_filters.BooleanFilter(
        widget=widgets.NullBooleanSelect(),
        lookup_expr="isnull",
        label="Active Position",
    )

    start_date = django_filters.DateFilter(
        widget=widgets.SelectDateWidget(),
        lookup_expr="gte",
        label="Started After",
    )

    class Meta:
        model = Leader
        fields = {
            "user__last_name": ["icontains"],
            "user__first_name": ["icontains"],
            "position__name": ["icontains"],
        }

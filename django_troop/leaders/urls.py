from django.urls import path
from django_filters.views import FilterView
from leaders.filters import LeaderFilter

from . import views

urlpatterns = [
    path("<int:pk>", views.LeaderDetailView.as_view(), name="leader_detail"),
    path("", views.LeaderListView.as_view(), name="leader_list"),
    path("update/<int:pk>/", views.LeaderUpdateView.as_view(), name="leader_update"),
    path(
        "start_leader/<int:position_pk>/",
        views.leader_create,
        name="leader_create_w_position",
    ),
    path("edit/new/", views.LeaderCreateView.as_view(), name="leader_create"),
    path("delete/<int:pk>/", views.LeaderDeleteView.as_view(), name="leader_delete"),
    path("scout/", views.ScoutLeaderListView.as_view(), name="scout_leader_list"),
    path("adult/", views.AdultLeaderListView.as_view(), name="adult_leader_list"),
    path(
        "filtered/",
        FilterView.as_view(filterset_class=LeaderFilter),
        name="filtered_leaders_list",
    ),
]

from badges.models import Badge
from django.test import TestCase
from icecream import ic
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff

# Create your tests here.


BADGE = {
    "name": "Tenderfoot",
    "badge_type": "rank",
    "order": 3,
}

SCOUT = {
    "last_name": "Perkins",
    "first_name": "Greg",
    "user_type": "scout",
}


REQUIREMENTS = [
    {
        "code": "7a",
        "text": "foobar",
    },
    {
        "code": "08",
        "text": "barfoo",
    },
]


class SignoffFinishRankTest(TestCase):
    def setUp(self):
        self.badge, _ = Badge.objects.get_or_create(**BADGE)
        self.requirements = [
            Requirement.objects.create(**data, badge=self.badge)
            for data in REQUIREMENTS
        ]
        self.scout, _ = User.objects.get_or_create(**SCOUT)

        self.badge.final = self.requirements[1]
        self.badge.save()

        self.signoffs = [
            Signoff.objects.create(
                requirement=self.requirements[0], scout=self.scout, date=None
            ),
            Signoff.objects.create(
                requirement=self.requirements[1], scout=self.scout, signed=True
            ),
        ]

        for signoff in self.signoffs:
            signoff.save()

    def test_scout_completed_rank(self):
        self.assertTrue(self.scout.badge_complete(self.badge))

    def test_badge_completed_scout(self):
        self.assertTrue(self.badge.complete(self.scout))

    def test_scout_not_completed_rank(self):
        self.signoffs[1].signed = False
        ic(self.signoffs[1])
        self.signoffs[1].save()
        self.assertFalse(self.scout.badge_complete(self.badge))

    def test_badge_not_completed_scout(self):
        self.signoffs[1].signed = False
        ic(self.signoffs[1])
        self.signoffs[1].save()
        self.assertFalse(self.badge.complete(self.scout))

    def test_all_requirements_completed(self):
        self.badge.final = None
        self.badge.save()

        self.signoffs[0].signed = True
        self.signoffs[0].save()

        self.assertTrue(self.scout.badge_complete(self.badge))
        self.assertTrue(self.badge.complete(self.scout))

from django.urls import path

from . import views

urlpatterns = [
    path("update/", views.signoff_update, name="signoff_update"),
    path("delete/", views.signoff_delete, name="signoff_delete"),
]

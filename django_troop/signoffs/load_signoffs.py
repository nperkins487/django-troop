import csv
from datetime import datetime
from pathlib import Path

from badges.models import Badge
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from icecream import ic
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff
from tm_parser import Parser
from tqdm import tqdm

CONFIG_DIR = settings.CONFIG_DIR
REMOVE_PERIOD = str.maketrans("", "", ".")


RANK_EQUIVALENTS = {
    "2nd Bronze Palm": "Bronze Palm (2)",
    "2nd Gold Palm": "Gold Palm (2)",
    "2nd Silver Palm": "Silver Palm (2)",
}


def load_rank_signoffs(path, load_type):
    signoffs = [
        Signoff(requirement=requirement, scout=scout, signed=False, date=None)
        for scout in User.scouts.all()
        for requirement in Requirement.objects.filter(badge__badge_type="rank").all()
    ]

    Signoff.objects.bulk_create(
        signoffs,
        update_conflicts=True,
        update_fields=["date", "signed"],
        unique_fields=["requirement", "scout"],
    )

    match load_type:
        case "troopwebhost":
            with open(path / "rank_signoffs.csv", encoding="utf-8-sig") as f:
                number = len(f.readlines())
                f.seek(0)
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Signoffs", total=number):
                    load_troopwebhost_signoff(line)

        case "troopmaster":
            p = Parser(path / "tm_data.pdf")
            for scout in tqdm(
                p.scouts.values(), desc="Scouts-signoffs", total=len(p.scouts.values())
            ):
                load_troopmaster_signoffs(scout)

    finish_up()
    record_whole_badges()


def load_troopwebhost_signoff(line):
    if line["Date Earned"]:
        rank = Badge.objects.get(badge_type="rank", name=line["Rank"])
        name = User.name_from_string(line["Scout"])
        scout = User.scouts.get(
            last_name=name.get("last_name"),
            first_name=name.get("first_name"),
        )
        requirement = Requirement.objects.get(
            badge=rank, code=fix_code(line["Code"].strip())
        )

        signoff = Signoff.objects.get(scout=scout, requirement=requirement)

        signoff.date = datetime.strptime(line["Date Earned"], "%m/%d/%y").date()
        signoff.signed = True
        signoff.save()


def load_troopmaster_signoffs(scout):
    """given a scout set of data from the parser
    load all their rank-related signoffs"""
    scout_name = User.name_from_string(scout["Data"]["Name"])
    try:
        scout_obj = User.scouts.get(
            first_name=scout_name.get("first_name"),
            last_name=scout_name.get("last_name"),
        )
    except User.DoesNotExist:
        print(f"Scout: {scout_name} does not exist in load_troopmaster_signoffs")

    for rank_str, data in scout["Ranks"].items():
        if rank_str in RANK_EQUIVALENTS:
            rank_str = RANK_EQUIVALENTS[rank_str]
        if "Palm" in rank_str:
            continue
        rank, created = Badge.objects.get_or_create(badge_type="rank", name=rank_str)
        if created:
            print(f"non-standard rank created: {rank_str}")
        if "Requirements" in data:
            for code, date in data.get("Requirements").items():
                if not date:
                    continue
                try:
                    requirement, created = Requirement.objects.get_or_create(
                        badge=rank, code=code
                    )
                    if created:
                        print(f"new requirement created for {rank.name}, {code}")
                except ObjectDoesNotExist:
                    print(f"no requirement for badge: {rank} and code: {code}")
                    continue
                signoff, created = Signoff.objects.get_or_create(
                    scout=scout_obj,
                    requirement=requirement,
                )
                if "Date" in date and date["Date"]:
                    signoff.date = date["Date"]
                    signoff.signed = True
                signoff.save()


def load_scoutbook_signoffs(scout):
    scout_data = scout["Data"]
    first_name = scout_data.get("First Name")
    last_name = scout_data.get("Last Name")
    scout_obj = User.scouts.get(first_name=first_name, last_name=last_name)

    if (
        "Advancement" not in scout
        or scout["Advancement"] is None
        or "Ranks" not in scout["Advancement"]
    ):
        return None
    for rank_str, data in scout.get("Advancement").get("Ranks").items():
        if rank_str in RANK_EQUIVALENTS:
            rank_str = RANK_EQUIVALENTS[rank_str]
        rank, created = Badge.objects.get_or_create(badge_type="rank", name=rank_str)
        if created:
            print(f"non-standard rank created: {rank_str}")
        if "Requirements" in data:
            for code, requirement_data in data.get("Requirements").items():
                if not requirement_data:
                    continue
                try:
                    requirement, created = Requirement.objects.get_or_create(
                        badge=rank, code=fix_code(code.strip())
                    )
                    if created:
                        print(f"new requirement created for {rank.name}, {code}")
                except ObjectDoesNotExist:
                    print(f"no requirement for badge: {rank} and code: {code}")
                    continue
                signoff, created = Signoff.objects.get_or_create(
                    scout=scout_obj,
                    requirement=requirement,
                )
                if "Date" in requirement_data and requirement_data["Date"]:
                    signoff.date = requirement_data["Date"]
                    signoff.signed = True
                signoff.save()


def fix_code(string):
    """takes format codes formatted like scoutbook has them and returns them normalized"""
    # 1.a -> 1a
    string = string.translate(REMOVE_PERIOD)

    if string.isdigit() and len(string) == 1:
        # 1, 4 -> 01, 04
        return "0" + string
    elif string.isdigit() and len(string) == 2:
        # 10, 11, 12 -> 10, 11, 12
        return string
    elif len(string) == 2:
        # 1a, 1c, 5a -> 01a, 01c, 05a
        return "0" + string
    elif len(string) == 3:
        # 10a, 11c -> 10a, 11c
        return string
    else:
        raise TypeError(f"poorly formatted rank requirement code: {string}")


def record_whole_badges():
    for rank in Badge.objects.filter(badge_type="rank").all():
        for scout in User.scouts.all():
            try:
                signoff = Signoff.signed_requirements.get(
                    scout=scout, requirement=rank.final
                )
                scout.completed_ranks.add(signoff)
            except Signoff.DoesNotExist:
                continue


def start_a_badge(scout, badge):
    signoffs = []
    if Signoff.objects.filter(requirement__badge=badge, scout=scout).exists():
        return None
    for requirement in Requirement.objects.filter(badge=badge).all():
        signoffs.append(
            Signoff(requirement=requirement, scout=scout, signed=False, date=None)
        )
    Signoff.objects.bulk_create(signoffs)


def finish_up():
    for requirement in Requirement.objects.filter(badge__badge_type="rank").all():
        if requirement.equivalent_requirements:
            for scout in User.scouts.all():
                for signoff in Signoff.signed_requirements.filter(
                    scout=scout,
                    requirement__badge=requirement.badge,
                    requirement__code__in=requirement.equivalent_requirements.split(),
                ).all():
                    signoff.save()


class Command(BaseCommand):
    help = "Loads signoffs"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=CONFIG_DIR, type=str)

    def handle(self, *args, **kwargs):
        CONFIG_DIR = Path(kwargs["config_dir"])

        load_rank_signoffs(CONFIG_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=CONFIG_DIR, **kwargs)

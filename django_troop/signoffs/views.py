from datetime import date

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.template import loader
from icecream import ic
from signoffs.models import Signoff


@permission_required("signoffs.signoff_allowed")
def signoff_update(request):
    template = loader.get_template("signoffs/signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.date = date.today()
    signoff.signed = True
    signoff.save()

    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))


@permission_required("signoffs.signoff_allowed")
def signoff_delete(request):
    template = loader.get_template("signoffs/not_signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.date = None
    signoff.signed = False
    signoff.save()

    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))

DOCKER_COMPOSE := "docker compose"
DOCKER_COMPOSE_FILE := "django_troop/docker-compose.yml"
TROOP_DOCKER_COMPOSE_FILE := "django_troop/troop-docker-compose.yml"



help:
	@echo "create TROOP PORT -- create a new troop's files"
	@echo "nginx TROOP PORT -- create nginx proxy file and certbot"
	@echo "ssh-tunnel PORT REMOTE_SERVER LOCAL_SERVER -- start tunnel from remote server
	@echo "up TROOP - start TROOP docker containers"
	@echo "down TROOP -- shut down TROOP docker containers"
	@echo "logs TROOP"
	@echo "shell TROOP"
	@echo "createsuperuser TROOP USERNAME EMAIL -- create a new superuser"
	@echo "manage TROOP *ARGS" 
	@echo "lint -- lint the project (black, ruff, isort)"
	@echo "list -- list docker containers"


create TROOP PORT:
	django_troop/scripts/setup_troop.sh {{TROOP}} {{PORT}}

nginx TROOP PORT:
	django_troop/scripts/setup_troop_nginx.sh {{TROOP}} {{PORT}}

ssh-tunnel PORT REMOTE_SERVER LOCAL_SERVER:
	ssh -f -N -R {{PORT}}:{{LOCAL_SERVER}}:{{PORT}} {{REMOTE_SERVER}}

up TROOP:
	export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env up -d --build

down TROOP:
	export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env down -v

logs TROOP:
	{export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env logs -f

shell TROOP:
	{export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env exec web python manage.py shell_plus

createsuperuser TROOP USERNAME EMAIL:
	{export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env exec web python manage.py createsuperuser --username {{USERNAME}} --email {{EMAIL}} 

manage TROOP *ARGS:
	{export COMPOSE_PROJECT_NAME={{TROOP}}; {{DOCKER_COMPOSE}} -f {{TROOP_DOCKER_COMPOSE_FILE}} --env-file django_troop/data/{{TROOP}}/.env exec web python manage.py {{ARGS}}

start-redis:
	{{ DOCKER_COMPOSE }} -f {{DOCKER_COMPOSE_FILE}} up -d --build

stop-redis:
	{{ DOCKER_COMPOSE }} -f {{DOCKER_COMPOSE_FILE}} down

lint:
	black . 
	ruff check . --ignore=F401
	isort . 

list: 
	docker ps
